//
//  MapViewController.m
//  PicPal
//
//  Created by Geppy Parziale on 11/24/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

#import "MapViewController.h"


@interface MapViewController () <MKMapViewDelegate>
@property IBOutlet MKMapView *mapView;
@end


@implementation MapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.mapView = nil;
}


@end
