//
//  PicsTableViewCell.h
//  PicPal
//
//  Created by Geppy Parziale on 11/24/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

@import UIKit;

@interface PicsTableViewCell : UITableViewCell
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *urlLabel;
@end
