//
//  PicInfo.m
//  PicPal
//
//  Created by Geppy Parziale on 11/24/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

#import "PicInfo.h"


@implementation PicInfo

@dynamic title;
@dynamic thumbnailUrl;
@dynamic photoID;
@dynamic secret;
@dynamic server;
@dynamic farm;
@dynamic location;
@dynamic fullImageUrl;

@end
