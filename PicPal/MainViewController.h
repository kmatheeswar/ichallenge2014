//
//  MainViewController.h
//  PicPal
//
//  Created by Geppy Parziale on 11/24/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

@import UIKit;
@import CoreData;


@interface MainViewController : UIViewController
@property NSManagedObjectContext *moc;
@end
