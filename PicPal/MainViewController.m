//
//  MainViewController.m
//  PicPal
//
//  Created by Geppy Parziale on 11/24/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

#import "MainViewController.h"
#import "ImageViewController.h"
#import "PicsTableViewCell.h"
#import "PicInfo.h"


// Go to https://www.flickr.com/services/developer/api/ and register your app to get a Flickr API Key
#define kFlickrKey      @"YOUR API KEY"

#define kSearchWord     @"landscape"
#define kTotalItems     200


@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>
@property IBOutlet UITableView *tableView;
@property NSURLSession *urlSession;
@property NSURLSessionDataTask *downloadTask;
@property (nonatomic) NSFetchedResultsController *fetchedResultsController;
@end


@implementation MainViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Memory Warnings
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.tableView = nil;
}

#pragma mark - Storyboard segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    PicsTableViewCell *cell = (PicsTableViewCell *)sender;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if ([segue.identifier isEqualToString:@"toImageViewController"]) {
        PicInfo *picInfo = self.fetchedResultsController.fetchedObjects[indexPath.row];
        NSURL *imageUrl = [NSURL URLWithString:picInfo.fullImageUrl];
        ImageViewController *vc = (ImageViewController *)segue.destinationViewController;
        [vc setImageURL:imageUrl];
    }
}


#pragma mark - Button actions
- (IBAction)updateButton:(id)sender
{
    [self updateData];
}

#pragma mark - Table view datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[self.fetchedResultsController sections] count] > 0) {
        id<NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    }
    else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PicsTableViewCell *cell = (PicsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"com.invasivecode.cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(PicsTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    PicInfo *picInfo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.titleLabel.text = [picInfo title];
    cell.urlLabel.text = [picInfo fullImageUrl];
}

#pragma marrk - Core Data Operations
- (void)updateData
{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.urlSession = [NSURLSession sessionWithConfiguration:config];

    NSString *urlString = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&text=%@&has_geo=1&per_page=%d&format=json&nojsoncallback=1", kFlickrKey, kSearchWord, kTotalItems];
    NSURL *url = [NSURL URLWithString:urlString];

    self.downloadTask = [self.urlSession dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data.length > 0) {

            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSArray *photoList = dictionary[@"photos"][@"photo"];

            for (id element in photoList) {

                PicInfo *picInfo = [NSEntityDescription insertNewObjectForEntityForName:@"PicInfo" inManagedObjectContext:self.moc];
                picInfo.farm = [element[@"farm"] intValue];
                picInfo.server = [element[@"server"] intValue];
                picInfo.secret = element[@"secret"];
                picInfo.photoID = [element[@"id"] longLongValue];
                picInfo.title = element[@"title"];
                picInfo.thumbnailUrl = [NSString stringWithFormat:@"http://farm%ld.staticflickr.com/%ld/%lld_%@_%@.jpg", (long)picInfo.farm, (long)picInfo.server, picInfo.photoID, picInfo.secret, @"m"];
                picInfo.fullImageUrl = [NSString stringWithFormat:@"http://farm%ld.staticflickr.com/%ld/%lld_%@_%@.jpg", (long)picInfo.farm, (long)picInfo.server, picInfo.photoID, picInfo.secret, @"b"];
                picInfo.location = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.geo.getLocation&api_key=%@&photo_id=%lld&format=json", kFlickrKey, picInfo.photoID];

            }
            [self.moc save:nil];
            [self fetchData];
            [self.tableView reloadData];
        }
    }];

    [self.downloadTask resume];
}

- (void)fetchData
{
    [self.fetchedResultsController performFetch:nil];
}

#pragma mark - Fetched Results Controller
- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController) {
        return _fetchedResultsController;
    }

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"PicInfo" inManagedObjectContext:self.moc];
    [fetchRequest setEntity:entity];

    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"photoID" ascending:NO];
    [fetchRequest setSortDescriptors:@[ sortDescriptor ]];

    [fetchRequest setFetchBatchSize:16];

    NSFetchedResultsController *frc;
    frc = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                              managedObjectContext:self.moc
                                                sectionNameKeyPath:nil
                                                         cacheName:@"frc.root"];
    self.fetchedResultsController = frc;

    return _fetchedResultsController;
}

@end
