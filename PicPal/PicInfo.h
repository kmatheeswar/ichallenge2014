//
//  PicInfo.h
//  PicPal
//
//  Created by Geppy Parziale on 11/24/14.
//  Copyright (c) 2014 iNVASIVECODE, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PicInfo : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * thumbnailUrl;
@property (nonatomic) int64_t photoID;
@property (nonatomic, retain) NSString * secret;
@property (nonatomic) int32_t server;
@property (nonatomic) int32_t farm;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * fullImageUrl;

@end
