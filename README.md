#iOS MASTER CHALLENGE 2014

###PREREQUISITES
For this challenge you need the latest version of Xcode (currently Xcode 6.1.1).
You are allowed to use only Apple Development Tools for this challenge. DO NOT install any CocoPods or any third-party library.

###INSTRUCTIONS

1. Check out the app from this GIT repository: https://bitbucket.org/invasivecode/ichallenge2014.git
2. Create a new branch from the MASTER with the following name: <your first name>-<your last name>. This branch will be your PERSONAL MASTER BRANCH.
3. Push immediately the initial version of this new branch to the remote repository.
4. Start working on the app. If you need an additional branch, you can create it locally. Push to the repository only changes from your  PERSONAL MASTER BRANCH. Please, make sure you understood this process. If not, do not hesitate to contact us.
5. Make a push to your PERSONAL MASTER BRANCH every 2 hours during the master challenge. 
6. The final push should contain a running application, we can test on any iOS device.
7. Get a Flickr API Key from this webpage: https://www.flickr.com/services/developer/api/

IMPORTANT: READ THESE INSTRUCTIONS BEFORE YOU START TO CODE.


####APP DESCRIPTION
The provided app downloads geo-referenced pictures from flickr. The initial version that you get from us displays only the URL and the name of the image in a table view.

Selecting a cell, the app navigates to a new screen showing the corresponding picture with a wrong aspect ratio. The app has also a tab bar. The Map tab contains an empty map.

Feel free to add your touch to the app to make it more secure, high performing and more delightful to use.

###WHAT TO DO

#####DESIGN AND UX
- Restructure the application storyboard in the following way: remove the tab bar controller on the iPhone version, insert the MapViewController in the navigation between the MainViewController and the ImageViewController on the iPad version, add a new ContainerViewController after the MainViewController. The ContainerViewController must contain the MapViewController and the ImageViewController (you define how to organize them on the screen).
- In the MainViewController, replace the tableview with a collection view and use the Fetched Results Controller already provided in the original source code (inside the MainViewController.m file).
- The collection view must behave differently on iPhones and iPads:
	- On iPhones, the collection view must work as a slide show: only one image per screen must be visible and the user can scroll horizontally in a paginated fashion. 
	- On iPads, the collection view must show multiple images in a single screen organized in a grid and eventually scroll vertically. 
- Create a custom cell for the collection view. The custom cell must display the downloaded images with the correct aspect ratio.
- Add a custom transition between the MainViewController and MapViewController (the custom transition will be designed by you).
- Add auto layout everywhere. The application must run on any screen size and device both in portrait and landscape mode.
- The Map should show a pin associated with the image selected in the MainViewController. The pin will have a callout and tapping on it, one navigates to the ImageViewController.
- The ImageViewController must show the whole image full screen. The image must not scroll and it should be displayed with the correct aspect ratio when the Rotate button is pressed.
- After the first application launch, the app must always show the previously downloaded images when coming from the Not Running state.
- Prepare the app for localization to Italian, French, German and Spanish (DO NOT LOCALIZE THE APP).
- Any implemented UI/UX solutions making your final version original, delightful and pixel-perfect will be evaluated with extra points. 

#####PERFORMANCE AND TESTING
- The app should run even if no network connection is available.
- The app should be responsive and follow the Apple Human Interface Guideline.
- The Persistent Store should be ready for lightweight migration.
- Add unit testing to validate the managed object subclasses.
- Add asynchronous unit testing to validate the data obtained from Flickr.
- Correct and fix any property in the provided source code.
- Check for memory leaks and abandoned memory. If you find any memory issue, attach the Instruments trace to the project and push it to your personal master branch.
- Make sure that you never block the main thread and use GCD where appropriate.
- Redesign the persistent store accordingly to the new UI.
- Handle correctly any error and where possible, try to recover it.

#####SECURITY
- Store the Flickr API Key inside the keychain at the first application launch and retrieve it on demand.
- Make sure that the Persistent Store is always encrypted when the device is locked.

#####SWIFT
- If you prefer to use Swift as programming language, please do so, but keep the MainViewController in Objective-C.

Good luck.